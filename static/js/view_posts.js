var app = function() {

    var self = {};

    Vue.config.silent = false; // show all warnings

    // Extends an array
    self.extend = function(a, b) {
        for (var i = 0; i < b.length; i++) {
            a.push(b[i]);
        }
    };
	
    var enumerate = function(v) { var k=0; return v.map(function(e) {e._idx = k++;});};
	
	self.search = function() {
		var p_type = self.vue.post_type;
		var f_city = self.vue.form_city;
		
        $.getJSON(search_post_list_url,{
        	p_type :self.vue.post_type,
			f_city : self.vue.form_city
        },
            function(data) {
                // I am assuming here that the server gives me a nice list
                // of posts, all ready for display.
                self.vue.post_list = data.post_list;
				
                // Post-processing.
                self.process_posts();
            }
        );

		
	};
	
    self.process_posts = function() {
        // This function is used to post-process posts, after the list has been modified
        // or after we have gotten new posts. 
        // We add the _idx attribute to the posts. 
        enumerate(self.vue.post_list);
        // We initialize the smile status to match the like. 
        self.vue.post_list.map(function (e) {
            // I need to use Vue.set here, because I am adding a new watched attribute
            // to an object.  See https://vuejs.org/v2/guide/list.html#Object-Change-Detection-Caveats
            // The code below is commented out, as we don't have smiles any more. 
            // Replace it with the appropriate code for thumbs. 
            // // Did I like it? 
            // // If I do e._smile = e.like, then Vue won't see the changes to e._smile . 
			
			 
        });
    };
	
	self.full_image = function(post_idx) {
		var temp = post_idx;
        var p = self.vue.post_list[post_idx];
		
		window.location.href = p.img_url;
	}
	
	
    self.vue = new Vue({
        el: "#vue-div",
        delimiters: ['${', '}'],
        unsafeDelimiters: ['!{', '}'],
        data: {
			current_user: String(current_active_user),
            post_list: [],
			post_type:"",
			form_city:"",
			city_maxCount: 30,
			city_remainingCount: 30,
			

        },
        methods: {
			search:self.search,
			full_image:self.full_image,
			city_countdown: function() {
				this.city_remainingCount = this.city_maxCount - this.form_city.length;
					      //this.hasError = this.title_remainingCount < 0;
			},
        }

    });
	self.search();
	
    return self;
};

var APP = null;

// No, this would evaluate it too soon.
// var APP = app();

// This will make everything accessible from the js console;
// for instance, self.x above would be accessible as APP.x
jQuery(function(){APP = app();});