var app = function() {

    var self = {};

    Vue.config.silent = false; // show all warnings

    // Extends an array
    self.extend = function(a, b) {
        for (var i = 0; i < b.length; i++) {
            a.push(b[i]);
        }
    };
	

    // Enumerates an array.
    var enumerate = function(v) { var k=0; return v.map(function(e) {e._idx = k++;});};
	
    self.get_posts = function() {
        $.getJSON(get_my_post_list_url,
            function(data) {
                // I am assuming here that the server gives me a nice list
                // of posts, all ready for display.
                self.vue.post_list = data.post_list;
				
                // Post-processing.
                self.process_posts();
            }
        );
        console.log("I fired the get");
    };
	
	self.post_recovered = function(post_idx){
		var temp = post_idx;
        var p = self.vue.post_list[post_idx];
        $.post(recovered_post_url, {
			post_id: p.id,
            recovered: !p.recovered,
        });
		p._recovered = !p.recovered;
		self.vue.post_list.splice(post_idx, 1);
		self.vue.recovered_post_list.push(p);

	};
	
	self.full_image = function(post_idx) {
		var temp = post_idx;
        var p = self.vue.post_list[post_idx];
		
		window.location.href = p.img_url;
	}
	self.get_recovered_posts = function(){

		$.getJSON(get_recovered_post_list_url,
			function(data){
				self.vue.recovered_post_list = data.recovered_post_list;
				console.log(data.recovered_post_list.length);
				
                self.process_recovered_posts;
				
			}
			
		);
		console.log(self.vue.recovered_post_list.length);
		
	};
	
	self.process_recovered_posts = function() {
		
		enumerate(self.vue.recovered_post_list);
		
		self.vue.post_list.map(function (e){
			Vue.set(e, '_viewing', false);
		});
	};
	
    self.process_posts = function() {
        // This function is used to post-process posts, after the list has been modified
        // or after we have gotten new posts. 
        // We add the _idx attribute to the posts. 
        enumerate(self.vue.post_list);
        // We initialize the smile status to match the like. 
        self.vue.post_list.map(function (e) {
            // I need to use Vue.set here, because I am adding a new watched attribute
            // to an object.  See https://vuejs.org/v2/guide/list.html#Object-Change-Detection-Caveats
            // The code below is commented out, as we don't have smiles any more. 
            // Replace it with the appropriate code for thumbs. 
            // // Did I like it? 
            // // If I do e._smile = e.like, then Vue won't see the changes to e._smile . 
            Vue.set(e, '_title', e.post_title);
            Vue.set(e, '_post_gps', e.post_gps);
			Vue.set(e, '_recovered', e.recovered);
			Vue.set(e, '_editing', false);
			
			 
        });
    };
	
	//POST EDITING
	self.edit = function (post_idx) {
		var temp = post_idx;
        var p = self.vue.post_list[post_idx];
		p._editing = true;
		console.log(post_idx);
		
	};
	self.submit_edit = function (post_idx){
		var temp = post_idx;
        var p = self.vue.post_list[post_idx];
		p._editing = false;
		
        $.post(edit_post_url, {
			post_id: p.id,
            post_title: p.post_title,
			post_description:p.post_description,
			post_city:p.post_city,
        });	
	};
	
	self.delete_post = function (post_idx) {
		var p = self.vue.post_list[post_idx];
		self.vue.post_list.splice(post_idx, 1);
		$.post(delete_post_url,{
			post_id:p.id,
		});
	};
	
    // Complete as needed.
    self.vue = new Vue({
        el: "#vue-div",
        delimiters: ['${', '}'],
        unsafeDelimiters: ['!{', '}'],
        data: {
			current_user: String(current_active_user),
            post_list: [],
			recovered_post_list: [],
			title_maxCount: 50,
        	title_remainingCount: 50,
			description_maxCount: 300,
			description_remainingCount:300,
			location_remainingCount:40,
			city_maxCount: 30,
			city_remainingCount: 30,
    		form_title: '',
        	form_description: '',
			form_city: '',
			form_gps: '',

        },
        methods: {
			post_recovered: self.post_recovered,
			full_image:self.full_image,
			edit:self.edit,
			submit_edit:self.submit_edit,
			delete_post:self.delete_post,
			title_countdown: function() {
				this.title_remainingCount = this.title_maxCount - this.form_title.length;
			      //this.hasError = this.title_remainingCount < 0;
			},
			description_countdown: function() {
				this.description_remainingCount = this.description_maxCount - this.form_description.length;
				      //this.hasError = this.title_remainingCount < 0;
			},
			city_countdown: function() {
				this.city_remainingCount = this.city_maxCount - this.form_city.length;
					      //this.hasError = this.title_remainingCount < 0;
			},
        }

    });
    self.get_posts();
	self.get_recovered_posts();

    return self;
};

var APP = null;

// No, this would evaluate it too soon.
// var APP = app();

// This will make everything accessible from the js console;
// for instance, self.x above would be accessible as APP.x
jQuery(function(){APP = app();});